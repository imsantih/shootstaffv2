package me.imsanti.dev.shootstaffv2.utils;

import org.bukkit.ChatColor;

public class ColorUtils {

    public static String color(final String textToColor) {
        return ChatColor.translateAlternateColorCodes('&', textToColor);
    }
}
