package me.imsanti.dev.shootstaffv2.commands;

import me.imsanti.dev.shootstaffv2.ShootStaffV2;
import me.imsanti.dev.shootstaffv2.utils.ColorUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class StaffCommand implements CommandExecutor {

    private final ShootStaffV2 shootStaffV2;

    public StaffCommand(final ShootStaffV2 shootStaffV2) {
        this.shootStaffV2 = shootStaffV2;
    }
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(ColorUtils.color("&cThis command can only be executed by players."));
            return true;
        }

        final Player player = (Player) sender;
        if(!player.hasPermission("shootstaff.staff")) {
            player.sendMessage(ColorUtils.color("&cPermisos insuficientes."));
            return true;
        }

        shootStaffV2.getModeManager().toggleMode(player);
        return true;
    }
}
