package me.imsanti.dev.shootstaffv2.items.list;

import me.imsanti.dev.shootstaffv2.items.ModeItem;
import me.imsanti.dev.shootstaffv2.utils.ColorUtils;
import me.imsanti.dev.shootstaffv2.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WorldEdit implements ModeItem {

    private final ItemStack item = new ItemBuilder(Material.WOODEN_AXE).setDisplayName(ColorUtils.color("&eWorld Edit")).build();
    @Override
    public void execute(final Player player, final Player target) {
        return;
    }

    @Override
    public ItemStack getItem() {
        return item;
    }

    @Override
    public String getNoColorName() {
        return "WorldEdit";
    }

    @Override
    public int getSlot() {
        return 6;
    }
}
