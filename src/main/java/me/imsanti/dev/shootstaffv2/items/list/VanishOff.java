package me.imsanti.dev.shootstaffv2.items.list;

import me.imsanti.dev.shootstaffv2.ShootStaffV2;
import me.imsanti.dev.shootstaffv2.items.ModeItem;
import me.imsanti.dev.shootstaffv2.utils.ColorUtils;
import me.imsanti.dev.shootstaffv2.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class VanishOff implements ModeItem {

    private final ItemStack item = new ItemBuilder(Material.GRAY_DYE).setDisplayName(ColorUtils.color("&7Vanish &8(&cDisabled&8)")).build();
    private final ShootStaffV2 shootStaffV2;

    public VanishOff(final ShootStaffV2 shootStaffV2) {
        this.shootStaffV2 = shootStaffV2;
    }
    @Override
    public void execute(final Player player, final Player target) {
        shootStaffV2.getModeManager().getVanishPlayers().add(player);
        Bukkit.getOnlinePlayers().forEach(online -> {
            online.hidePlayer(player);
        });

        if(shootStaffV2.getModeManager().isInMode(player)) {
            player.getInventory().setItem(player.getInventory().getHeldItemSlot(), shootStaffV2.getModeManager().getItem("VanishOn").getItem());
            player.sendMessage(ColorUtils.color("&6Your Vanish Mode now is &aenabled"));

            return;
        }

        player.sendMessage(ColorUtils.color("&6Your Vanish Mode now is &aenabled"));
        return;
    }

    @Override
    public ItemStack getItem() {
        return item;
    }

    @Override
    public String getNoColorName() {
        return "VanishOff";
    }

    @Override
    public int getSlot() {
        return 8;
    }
}
