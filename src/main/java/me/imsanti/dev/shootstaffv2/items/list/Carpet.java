package me.imsanti.dev.shootstaffv2.items.list;

import me.imsanti.dev.shootstaffv2.items.ModeItem;
import me.imsanti.dev.shootstaffv2.utils.ColorUtils;
import me.imsanti.dev.shootstaffv2.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Carpet implements ModeItem {

    private final ItemStack item = new ItemBuilder(Material.ORANGE_CARPET).setDisplayName(ColorUtils.color("&e")).build();
    @Override
    public void execute(final Player player, final Player target) {
        return;
    }

    @Override
    public ItemStack getItem() {
        return item;
    }

    @Override
    public String getNoColorName() {
        return "Carpet";
    }

    @Override
    public int getSlot() {
        return 4;
    }
}
