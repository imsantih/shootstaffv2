package me.imsanti.dev.shootstaffv2.items.list;

import me.imsanti.dev.shootstaffv2.items.ModeItem;
import me.imsanti.dev.shootstaffv2.utils.ColorUtils;
import me.imsanti.dev.shootstaffv2.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Inspect implements ModeItem {

    private final ItemStack item = new ItemBuilder(Material.BOOK).setDisplayName(ColorUtils.color("&aInspect &7(Right-Click)")).build();
    @Override
    public void execute(final Player player, final Player target) {
        player.openInventory(target.getInventory());

        player.sendMessage(ColorUtils.color("&6Inspecting &f" + target.getName() + "&f's &6inventory."));
    }

    @Override
    public ItemStack getItem() {
        return item;
    }

    @Override
    public String getNoColorName() {
        return "Inspect";
    }

    @Override
    public int getSlot() {
        return 2;
    }
}
