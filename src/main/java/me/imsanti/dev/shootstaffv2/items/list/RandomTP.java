package me.imsanti.dev.shootstaffv2.items.list;

import me.imsanti.dev.shootstaffv2.items.ModeItem;
import me.imsanti.dev.shootstaffv2.utils.ColorUtils;
import me.imsanti.dev.shootstaffv2.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomTP implements ModeItem {

    private final ItemStack item = new ItemBuilder(Material.ENDER_PEARL).setDisplayName(ColorUtils.color("&6Random TP &7(Right-Click)")).build();
    @Override
    public void execute(final Player player, final Player target) {
        final List<Player> onlinePlayers = new ArrayList<>(Bukkit.getOnlinePlayers());
        onlinePlayers.remove(player);
        if(onlinePlayers.size() == 0) {
            player.sendMessage(ColorUtils.color("&6There is &fno sufficent &6players online to teleport you."));
            return;
        }
        final Player finded = onlinePlayers.get(new Random().nextInt(onlinePlayers.size()));

        player.teleport(finded.getLocation());

        player.sendMessage(ColorUtils.color("&6You have been random teleported to &f" + finded.getName() ));
    }

    @Override
    public ItemStack getItem() {
        return item;
    }

    @Override
    public String getNoColorName() {
        return "Random-TP";
    }

    @Override
    public int getSlot() {
        return 1;
    }
}
