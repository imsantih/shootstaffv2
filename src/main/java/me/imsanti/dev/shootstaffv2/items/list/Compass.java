package me.imsanti.dev.shootstaffv2.items.list;

import me.imsanti.dev.shootstaffv2.items.ModeItem;
import me.imsanti.dev.shootstaffv2.utils.ColorUtils;
import me.imsanti.dev.shootstaffv2.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Compass implements ModeItem {

    private final ItemStack item = new ItemBuilder(Material.COMPASS).setDisplayName(ColorUtils.color("&cCompass &7(Right-Click)")).build();
    @Override
    public void execute(final Player player, final Player target) {
        return;
    }

    @Override
    public ItemStack getItem() {
        return item;
    }

    @Override
    public String getNoColorName() {
        return "Compass";
    }

    @Override
    public int getSlot() {
        return 0;
    }
}
