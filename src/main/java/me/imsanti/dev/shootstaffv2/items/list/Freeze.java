package me.imsanti.dev.shootstaffv2.items.list;

import me.imsanti.dev.shootstaffv2.items.ModeItem;
import me.imsanti.dev.shootstaffv2.utils.ColorUtils;
import me.imsanti.dev.shootstaffv2.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Freeze implements ModeItem {

    private final ItemStack item = new ItemBuilder(Material.PACKED_ICE).setDisplayName(ColorUtils.color("&bFreeze &7(Right-Click)")).build();
    private final Map<UUID, Location> playersLocation = new HashMap<>();
    private final Location freezeLocation = new Location(Bukkit.getWorld("MundoRPG"), 0, 100, 0);

    @Override
    public void execute(final Player player, final Player target) {
        if(playersLocation.containsKey(target.getUniqueId())) {
            unfreeze(target);
            return;
        }

        freeze(target);
    }

    @Override
    public ItemStack getItem() {
        return item;
    }

    @Override
    public String getNoColorName() {
        return "Freeze";
    }

    @Override
    public int getSlot() {
        return 7;
    }

    public void freeze(final Player player) {
        playersLocation.put(player.getUniqueId(), player.getLocation());
        player.teleport(freezeLocation);
        player.sendMessage(ColorUtils.color("&eHas sido congelado por un Staff."));
    }

    public void unfreeze(final Player player) {
        player.teleport(playersLocation.get(player.getUniqueId()));
        playersLocation.remove(player.getUniqueId());
        player.sendMessage(ColorUtils.color("&aHas sido desfrozeado."));
    }
}
