package me.imsanti.dev.shootstaffv2.items;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface ModeItem {

    void execute(final Player player, final Player target);
    ItemStack getItem();
    String getNoColorName();
    int getSlot();

}
