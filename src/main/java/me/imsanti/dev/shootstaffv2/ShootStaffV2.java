package me.imsanti.dev.shootstaffv2;

import me.imsanti.dev.shootstaffv2.commands.StaffCommand;
import me.imsanti.dev.shootstaffv2.items.ModeItem;
import me.imsanti.dev.shootstaffv2.listeners.DenyListener;
import me.imsanti.dev.shootstaffv2.listeners.StaffListener;
import me.imsanti.dev.shootstaffv2.managers.ModeManager;
import me.imsanti.dev.shootstaffv2.managers.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public final class ShootStaffV2 extends JavaPlugin {

    private final ModeManager modeManager = new ModeManager(this);
    private final PlayerManager playerManager = new PlayerManager();
    private final List<ModeItem> loadedItems = new ArrayList<>();

    @Override
    public void onEnable() {
        registerCommands();
        registerEvents();
        new BukkitRunnable() {

            @Override
            public void run() {
                modeManager.loadItems();
            }
        }.runTaskLater(this, 3L);
        // Plugin startup logic

    }

    @Override
    public void onDisable() {
        modeManager.getModePlayers().forEach(modeManager::toggleMode);
        // Plugin shutdown logic
    }

    private void registerEvents() {
        Bukkit.getPluginManager().registerEvents(new StaffListener(this), this);
        Bukkit.getPluginManager().registerEvents(new DenyListener(this), this);
    }

    private void registerCommands() {
        getCommand("staff").setExecutor(new StaffCommand(this));
    }

    public List<ModeItem> getLoadedItems() {
        return loadedItems;
    }

    public ModeManager getModeManager() {
        return modeManager;
    }

    public PlayerManager getPlayerManager() {
        return playerManager;
    }
}

