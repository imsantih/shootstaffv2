package me.imsanti.dev.shootstaffv2.listeners;

import me.imsanti.dev.shootstaffv2.ShootStaffV2;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class DenyListener implements Listener {

    private final ShootStaffV2 shootStaffV2;

    public DenyListener(final ShootStaffV2 shootStaffV2) {
        this.shootStaffV2 = shootStaffV2;
    }

    @EventHandler
    private void handleItemDrop(final PlayerDropItemEvent event) {
        if(!shootStaffV2.getModeManager().isInMode(event.getPlayer())) return;

        if(shootStaffV2.getModeManager().isModeItem(event.getItemDrop().getItemStack())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    private void handleInventoryClick(final InventoryClickEvent event) {
        try {
            if(shootStaffV2.getModeManager().isInMode((Player) event.getWhoClicked())) {
                event.setCancelled(true);
            }
        }catch (Exception exception) {
            return;
        }
    }

    @EventHandler
    private void handlePickUp(final PlayerPickupItemEvent event) {
        if(!shootStaffV2.getModeManager().isInMode(event.getPlayer())) return;
        event.setCancelled(true);
    }


}
