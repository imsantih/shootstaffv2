package me.imsanti.dev.shootstaffv2.listeners;

import me.imsanti.dev.shootstaffv2.ShootStaffV2;
import me.imsanti.dev.shootstaffv2.items.ModeItem;
import org.bukkit.GameMode;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class StaffListener implements Listener {

    private final ShootStaffV2 shootStaffV2;

    public StaffListener(final ShootStaffV2 shootStaffV2) {
        this.shootStaffV2 = shootStaffV2;
    }

    @EventHandler
    private void handleVanish(final PlayerJoinEvent event) {
        if(event.getPlayer().hasPermission("shootstaff.staff")) return;
        shootStaffV2.getModeManager().getVanishPlayers().forEach(vanished -> {
            event.getPlayer().hidePlayer(vanished);
        });
    }

    @EventHandler
    private void handleItemClick(PlayerInteractEvent event) {
        if(!(event.getAction() == Action.RIGHT_CLICK_AIR) && !(event.getAction() == Action.RIGHT_CLICK_BLOCK)) return;
        if(!event.getPlayer().hasPermission("shootstaff.staff")) return;
        if(!shootStaffV2.getModeManager().isInMode(event.getPlayer())) return;
        event.setCancelled(true);

        if(!shootStaffV2.getModeManager().isModeItem(event.getPlayer().getInventory().getItemInMainHand())) return;
        final ModeItem modeItem = shootStaffV2.getModeManager().getItem(event.getPlayer().getInventory().getItemInMainHand());
        if(modeItem == null) {
            return;
        }

        if(modeItem.getNoColorName().equalsIgnoreCase("Freeze") || modeItem.getNoColorName().equalsIgnoreCase("Inspect")) return;
        modeItem.execute(event.getPlayer(), null);

    }

    @EventHandler
    private void handleTargetItemClick(final PlayerInteractEntityEvent event) {
        if(!(event.getRightClicked().getType() == EntityType.PLAYER)) return;
        if(!event.getPlayer().hasPermission("shootstaff.staff")) return;
        if(!shootStaffV2.getModeManager().isInMode(event.getPlayer())) return;
        event.setCancelled(true);

        if(!shootStaffV2.getModeManager().isModeItem(event.getPlayer().getInventory().getItemInMainHand())) return;

        final ModeItem modeItem = shootStaffV2.getModeManager().getItem(event.getPlayer().getInventory().getItemInMainHand());
        if(modeItem.getNoColorName().equalsIgnoreCase("Freeze") || modeItem.getNoColorName().equalsIgnoreCase("Inspect")) {
            modeItem.execute(event.getPlayer(), (Player) event.getRightClicked());
        }

    }

    @EventHandler
    private void handleStaffQuit(PlayerQuitEvent event) {
        if(!event.getPlayer().hasPermission("shootstaff.staff")) return;

        if(shootStaffV2.getModeManager().isInMode(event.getPlayer())) {
            shootStaffV2.getModeManager().toggleMode(event.getPlayer());
        }
    }

    @EventHandler
    private void handleGamemodeChange(final PlayerInteractEvent event) {
        if(event.getAction() == Action.LEFT_CLICK_AIR && event.getPlayer().isSneaking() && shootStaffV2.getModeManager().isInMode(event.getPlayer())) {
            switch (event.getPlayer().getGameMode()) {
                case SPECTATOR:
                    event.getPlayer().setGameMode(GameMode.CREATIVE);
                    break;
                case CREATIVE:
                    event.getPlayer().setGameMode(GameMode.SPECTATOR);
                    break;
                default:
                    return;
            }
        }
    }

    @EventHandler
    private void handleStaffDamage(final EntityDamageEvent event) {
        if(!(event.getEntityType() == EntityType.PLAYER)) return;

        final Player target = (Player) event.getEntity();
        if(!shootStaffV2.getModeManager().isInMode(target)) return;

        event.setCancelled(true);
    }
}
