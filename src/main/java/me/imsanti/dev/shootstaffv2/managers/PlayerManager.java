package me.imsanti.dev.shootstaffv2.managers;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerManager {

    private final Map<UUID, Map<Integer, ItemStack>> playersItems = new HashMap<>();

    public void saveInventory(final Player player) {
        int slot = 0;
        final Map<Integer, ItemStack> items = new HashMap<>();
        for(ItemStack item : player.getInventory().getContents()) {
            items.put(slot, item);
            slot++;
        }
        playersItems.put(player.getUniqueId(), items);
    }

    public void rollbackInventory(final Player player) {
        player.getInventory().clear();
        for (int i = 0; i < playersItems.get(player.getUniqueId()).values().size(); i++) {
            player.getInventory().setItem(i, playersItems.get(player.getUniqueId()).get(i));
        }
    }



}
