package me.imsanti.dev.shootstaffv2.managers;

import me.imsanti.dev.shootstaffv2.ShootStaffV2;
import me.imsanti.dev.shootstaffv2.items.ModeItem;
import me.imsanti.dev.shootstaffv2.items.list.*;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class ModeManager {

    private final List<Player> modePlayers = new ArrayList<>();
    private final List<Player> vanishPlayers = new ArrayList<>();
    private final ShootStaffV2 shootStaffV2;

    public ModeManager(final ShootStaffV2 shootStaffV2) {
        this.shootStaffV2 = shootStaffV2;
    }
    public void toggleMode(final Player player) {
        if(modePlayers.contains(player)) {
            shootStaffV2.getPlayerManager().rollbackInventory(player);
            modePlayers.remove(player);
            player.setGameMode(GameMode.SURVIVAL);
            player.setAllowFlight(false);
            return;
        }

        shootStaffV2.getPlayerManager().saveInventory(player);
        player.getInventory().clear();
        player.setGameMode(GameMode.CREATIVE);
        player.setAllowFlight(true);
        giveItems(player);

        modePlayers.add(player);
    }

    public void loadItems() {
        shootStaffV2.getLoadedItems().add(new RandomTP());
        shootStaffV2.getLoadedItems().add(new Compass());
        shootStaffV2.getLoadedItems().add(new Inspect());
        shootStaffV2.getLoadedItems().add(new VanishOff(shootStaffV2));
        shootStaffV2.getLoadedItems().add(new VanishOn(shootStaffV2));
        shootStaffV2.getLoadedItems().add(new WorldEdit());
        shootStaffV2.getLoadedItems().add(new Freeze());
        shootStaffV2.getLoadedItems().add(new Carpet());
    }

    public void giveItems(final Player player) {
        for(final ModeItem modeItem : shootStaffV2.getLoadedItems()) {
            if(modeItem.getNoColorName().equalsIgnoreCase("VanishOn")) continue;

            player.getInventory().setItem(modeItem.getSlot(), modeItem.getItem());
        }

    }

    public boolean isInMode(final Player player) {
        return modePlayers.contains(player);
    }

    public List<Player> getVanishPlayers() {
        return vanishPlayers;
    }

    public ModeItem getItem(final String noColorName) {
        for(final ModeItem modeItem : shootStaffV2.getLoadedItems()) {
            if(modeItem.getNoColorName().equalsIgnoreCase(noColorName)) {
                return modeItem;
            }
        }
        return null;
    }

    public boolean isModeItem(final ItemStack stack) {
        for(final ModeItem modeItem : shootStaffV2.getLoadedItems()) {
            if(modeItem.getItem().isSimilar(stack)) return true;
        }
        return false;
    }

    public ModeItem getItem(final ItemStack stack) {
        for(final ModeItem modeItem : shootStaffV2.getLoadedItems()) {
            if(modeItem.getItem().isSimilar(stack)) {
                return modeItem;
            }
        }
        return null;
    }

    public List<Player> getModePlayers() {
        return modePlayers;
    }
}
